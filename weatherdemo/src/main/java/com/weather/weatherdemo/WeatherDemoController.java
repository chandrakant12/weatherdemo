package com.weather.weatherdemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.weather.weatherdemo.service.PredicateService;


@RestController
@RequestMapping("/weather")
public class WeatherDemoController {
	private final static Logger LOGGER = LoggerFactory.getLogger(WeatherDemoController.class);
	@Autowired
	private PredicateService predicateService_;
	@RequestMapping(value = "/{zipcode}", method = RequestMethod.GET)
	  public String getCoolestTempByZipCode(@PathVariable("zipcode") int zipcode){
		LOGGER.info("Zip-Code "+zipcode);
		return predicateService_.getDetails(zipcode);
	  }
}
