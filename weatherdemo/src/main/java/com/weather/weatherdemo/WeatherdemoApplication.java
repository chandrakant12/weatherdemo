package com.weather.weatherdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherdemoApplication {
	private final static Logger LOGGER = LoggerFactory.getLogger(WeatherdemoApplication.class);
	public static void main(String[] args) {
		LOGGER.info("Starting Application");
		SpringApplication.run(WeatherdemoApplication.class, args);
	}
}