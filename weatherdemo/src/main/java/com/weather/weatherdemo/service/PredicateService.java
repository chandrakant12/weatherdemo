package com.weather.weatherdemo.service;

import org.springframework.stereotype.Service;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
@Service
public class PredicateService {
	private final static Logger LOGGER = LoggerFactory.getLogger(PredicateService.class);
	public String getDetails(int zipcode) {
		try{
		RestTemplate restTemplate = new RestTemplate();
		URL url = new URL("https://weather.cit.api.here.com/weather/1.0/report.json?product=forecast_hourly&app_id=NHqC0F77rf79RBfr6A25&app_code=e4ibwkB5feb9XHByQHqSkw&zipcode=" + zipcode);
		LOGGER.info("url " + url.toString());
		JSONObject obj = null;
		obj = new JSONObject(restTemplate.getForObject(url.toString(), String.class));
		JSONArray arr = null;
		arr = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getJSONArray("forecast");
		String cityName = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getString("city");
		
		ArrayList<Double>  temp = new ArrayList<Double>();
		ArrayList<String> dateTime = new ArrayList<String>();
		int i = 0;
		String today = arr.getJSONObject(i).getString("weekday");
		for (i=1;today.equals(arr.getJSONObject(i).getString("weekday"));i++)
		{}
		for (int j=0; j < 24; i++,j++)
		{
			temp.add(Double.parseDouble((arr.getJSONObject(i).getString("temperature"))));
			dateTime.add(arr.getJSONObject(i).getString("localTime"));
		}
		int min_index = temp.indexOf(Collections.min(temp));
		String time = dateTime.get(min_index).substring(0,2);
		String date = dateTime.get(min_index).substring(2, dateTime.get(min_index).length());
		date = date.substring(0,2) + "-" + date.substring(2,4) + "-" + date.substring(4,8);
		String meridiem = "am";
		//Display time in meridiem
		if(Integer.parseInt(time) > 11)
		{
			if(time!="12")
				time = Integer.toString((Integer.parseInt(time) - 12));
			meridiem = "pm";
		} else {
			if(time=="00")
				time = "12";
		}
		LOGGER.info("url" + "coolest hour in " + cityName + " tomorrow on " + date + " would be at " + time + " " + meridiem + " and the temperature would be " + temp.get(min_index) +  " F\n");
		//Finds the coolest hour of tomorrow for the city of user feeded zipcode
		return "coolest hour in " + cityName + " tomorrow on " + date + " would be at " + time + " " + meridiem + " and the temperature would be " + temp.get(min_index) +  " F\n" ;
		}catch (IOException e) {
			LOGGER.error("error" + e.getStackTrace());
			return "City not found, please enter a valid Zip Code" + e.getMessage();
		}catch (JSONException e) {
			LOGGER.error("error" + e.getStackTrace());
			return " Invalid API format " + e.getMessage();
		}catch(Exception e){
			LOGGER.error("error" + e.getStackTrace());
			return "Bad Request" + e.getMessage();
		}
	}
}
