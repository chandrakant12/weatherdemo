package com.weather.weatherdemo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.weatherdemo.service.PredicateService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherdemoApplicationTests {
	private final static Logger LOGGER = LoggerFactory.getLogger(WeatherdemoApplicationTests.class);
    @Autowired
    private PredicateService predicateService_;
	@Test
	public void testgetDetailsWithValiadZipCode(){
	String data = predicateService_.getDetails(10025);
	LOGGER.info("Passed");
	assertTrue(data.startsWith("coolest"));
	}
	@Test
	public void testgetDetailsWithInvalidZipCode(){
	String data = predicateService_.getDetails(100);
	LOGGER.info("Passed");
	assertTrue(data.contains("Bad Request"));
	}
}
